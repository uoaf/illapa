#!/bin/bash

IMAGENAME="pgsql-custom-image"
CONTAINERNAME="pgsql-container"

# Check if the container is already running
CONTAINER_ID=`docker ps | grep $IMAGENAME | cut -d ' ' -f 1`

if [ -z "${CONTAINER_ID}" ]; then

  docker run \
    --name $CONTAINERNAME -d $IMAGENAME -p 6379:6379

fi

docker ps