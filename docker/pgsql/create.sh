#!/bin/bash

# Create a customized PostgreSQL docker image from the official postgress image.
# This has our own postgress.conf file in it.

FROMIMAGENAME="postgres:10.3"   
IMAGENAME="pgsql-custom-image"

docker build -t $IMAGENAME - << EOF
FROM $FROMIMAGENAME


EOF
