#!/bin/bash

# Create a customized redis docker image from the official redis image.
# This is supposed to have our own redis.conf file in it.

# See alos:
#   https://github.com/dockerfile/redis/blob/master/Dockerfile


FROMIMAGENAME="redis"   
IMAGENAME="redis-custom-image"
CONTAINERNAME="redis-container"

docker build -t $IMAGENAME -f - . << EOF
FROM $FROMIMAGENAME

# COPY redis.conf /etc/redis/redis.conf

EXPOSE 6379

EOF
