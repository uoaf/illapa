#!/bin/bash

IMAGENAME="redis-custom-image"
CONTAINERNAME="redis-container"

# Check if the container is already running
CONTAINER_ID=`docker ps | grep $IMAGENAME | cut -d ' ' -f 1`

if [ -z "${CONTAINER_ID}" ]; then

docker run \
	-d -p 6379:6379 \
    --name $CONTAINERNAME $IMAGENAME 

fi
