#!/bin/bash

CONTAINERNAME="illapa"

IMAGENAME="illapa"

IMAGENAME_BASE="illapa_base"

docker rm $CONTAINERNAME

docker rmi -f $IMAGENAME

docker rmi -f $IMAGENAME_BASE
