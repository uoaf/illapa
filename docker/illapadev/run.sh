#!/bin/bash

CONTAINERNAME="illapa"

[ ! "$(docker ps -aq -f name=$CONTAINERNAME -f status=running)" ] && docker start $CONTAINERNAME >/dev/null

docker exec -it -e COLUMNS=$(tput cols) -e LINES=$(tput lines) -w=$PWD $CONTAINERNAME bash "$@"
