#!/bin/bash

IMAGENAME_BASE="illapa_base"
IMAGENAME="illapa"

USERID=$(id -u)

# Build the Dockerfile
docker build -t $IMAGENAME_BASE .

# Build from stdin, and add the host user.
docker build -t $IMAGENAME - << EOF
FROM $IMAGENAME_BASE

RUN useradd -ms /bin/bash $USER -u $USERID
ARG DEBIAN_FRONTEND=noninteractive

# The host user is the same user inside container 
RUN  mkdir -p /home/$USER && mkdir -p /home/$USER/.cache && \
    echo "$USER:x:1000:1000:$USER,,,:/home/$USER:/bin/bash" >> /etc/passwd && \
    echo "$USER:x:1000:" >> /etc/group && \
    echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER && \
    chmod 0440 /etc/sudoers.d/$USER && \
    chown $USER:$USER -R /home/$USER

USER $USER
ENV HOME /home/$USER
WORKDIR /home/$USER/

EOF


