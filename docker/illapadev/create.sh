#!/bin/bash

IMAGENAME="illapa"
CONTAINERNAME="illapa"

touch $HOME/
touch $HOME/.bashrc
touch $HOME/.vimrc

docker create -it --name $CONTAINERNAME \
  --net host \
  --privileged \
  --shm-size 4G \
  --user $USER \
  -v $HOME/:$HOME \
  -v $HOME/.bashrc:$HOME/.bashrc \
  -v $HOME/.bash_history:$HOME/.bash_history \
  -v $HOME/.vimrc:$HOME/.vimrc \
  -v /etc/hosts:/etc/hosts \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v /var/run/dbus:/var/run/dbus \
  -e DISPLAY=$DISPLAY \
  -e CONTAINER_NAME=$CONTAINERNAME \
  -h $CONTAINERNAME $IMAGENAME

